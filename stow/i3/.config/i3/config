# i3 config file (v4)

########################################
# SCRIPTS
########################################
# launch alternating layout scripts
exec_always --no-startup-id $HOME/.config/i3/alternating_layouts.py

# launch polybar
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

########################################
# VISUAL
########################################
font pango:Roboto 11
for_window [class="^.*"] border pixel 1
gaps inner 10
gaps outer 20

########################################
# BEHAVIOR
########################################
focus_follows_mouse no

########################################
# COLORS
########################################
# import from Xresources
set_from_resource $foreground foreground
set_from_resource $background background
set_from_resource $gray color8
set_from_resource $red color1
set_from_resource $yellow color3

# class                 border, background, text, indicator, child_border
client.focused          $background $background $foreground $yellow $yellow
client.focused_inactive $background $background $foreground $gray $gray
client.unfocused        $background $background $foreground $gray $gray
client.urgent           $background $background $foreground $red $red
client.placeholder      $background $yellow $foreground $gray $gray

client.background       $background

########################################
# BINDINGS
########################################
# set modifier to alt
set $mod Mod1

# restart i3
bindsym $mod+Shift+r restart

# kill window
bindsym $mod+Shift+q kill

# launch terminal
bindsym $mod+Return exec --no-startup-id alacritty

# media controls
bindsym XF86AudioPlay exec playerctl play
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec light -A 5 # increase screen brightness
bindsym XF86MonBrightnessDown exec light -U 5 # decrease screen brightness

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -q set Master 5%+ unmute #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -q set Master 5%- unmute #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound

# rofi
bindsym $mod+space exec --no-startup-id rofi -show run

# toggle fullscreen
bindsym $mod+f fullscreen toggle

# toggle floating/tiling
bindsym $mod+t floating toggle

# movement
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# split orientation
bindsym $mod+c split h
bindsym $mod+v split v

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

########################################
# Modes
########################################
# resize window (you can also use the mouse for that)
set $mode_resize Resize window: shrink/grow with h/l (width), j/k (height)
bindsym $mod+r mode "$mode_resize"
mode "$mode_resize" {
    bindsym h resize shrink width 10 px or 10 ppt
    bindsym j resize grow height 10 px or 10 ppt
    bindsym k resize shrink height 10 px or 10 ppt
    bindsym l resize grow width 10 px or 10 ppt

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# resize gaps
set $mode_gaps Resize outer gap: +/-/0 (local), Shift + +/-/0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"
mode "$mode_gaps" {
    bindsym plus  gaps outer current plus 5
    bindsym minus gaps outer current minus 5
    bindsym 0     gaps outer current set 20

    bindsym Shift+plus  gaps outer all plus 5
    bindsym Shift+minus gaps outer all minus 5
    bindsym Shift+0     gaps outer all set 20

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

