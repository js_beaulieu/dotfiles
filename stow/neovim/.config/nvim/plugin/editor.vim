" turn off vi compatibility
set nocompatible

" wildmenu
set wildmenu
set wildmode=longest:full,full

" tabs
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" indentation based on filetype
filetype plugin indent on

" search
set incsearch
set hlsearch

" mouse mode on
set mouse=a

" disable backup and swap files
set nobackup
set noswapfile

" default encoding
set enc=utf-8

" don't auto-conceal stuff
set conceallevel=0

" disable netrw history
let g:netrw_dirhistmax = 0
