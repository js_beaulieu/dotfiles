""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This `init.vim` config file only loads plugins and their settings.
" Further configuration files are in the `./plugin` folder.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')
"Plug 'joshdick/onedark.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'sheerun/vim-polyglot'
Plug 'ntpeters/vim-better-whitespace'
Plug 'christoomey/vim-tmux-navigator'
Plug 'airblade/vim-gitgutter'
Plug 'easymotion/vim-easymotion'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' }
Plug 'carlitux/deoplete-ternjs'
Plug 'zchee/deoplete-jedi'
Plug 'wellle/tmux-complete.vim'
Plug 'itchyny/lightline.vim'
Plug 'w0rp/ale'
Plug 'srstevenson/vim-picker'
Plug 'joonty/vdebug'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-rooter'
Plug 'jiangmiao/auto-pairs'
Plug 'Yggdroot/indentLine'
Plug 'mgee/lightline-bufferline'
Plug 'tpope/vim-commentary'
call plug#end()


" vim-better-whitespace (whitespace management)
autocmd BufWritePre * StripWhitespace

" deoplete.nvim (autocompletion)
let g:deoplete#enable_at_startup = 1
call deoplete#custom#source('ultisnips', 'matchers', ['matcher_fuzzy'])

" lightline.vim (better status bar)
let g:lightline = { 'colorscheme': 'one' }
let g:lightline.tabline = {'left': [['buffers']], 'right': [['']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
let g:lightline.component_type = {'buffers': 'tabsel'}

" lightline-bufferline
let g:lightline#bufferline#show_number = 2
let g:lightline#bufferline#enable_devicons = 1
let g:lightline#bufferline#unnamed = '[No Name]'
function! LightlineBufferline()
  call bufferline#refresh_status()
  return [ g:bufferline_status_info.before, g:bufferline_status_info.current, g:bufferline_status_info.after]
endfunction

" ale (linter)
let g:ale_fixers = {
    \'javascript': ['eslint'],
    \'php': ['phpcs'],
\}

" vim-easymotion
let g:EasyMotion_smartcase = 1
let g:EasyMotion_startofline = 0

" twig
" au BufNewFile,BufRead *.html setlocal ft=twig.html
" au BufNewFile,BufRead *.htm setlocal ft=twig.html

" ultiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" commentary.vim
autocmd FileType twig setlocal commentstring={#\ %s\ #}'
